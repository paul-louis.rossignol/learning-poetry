# Poetry learning
## How to import private repo with poetry
https://python-poetry.org/docs/cli/#add

Create poetry project
```
poetry new project-name
```
Add your distant repo 
```
poetry add "git+ssh://git@gitlab.com:paul-louis.rossignol/utils-project.git"
```
or
```
poetry add git+https://github.com/sdispater/pendulum.git
```
## Errors
If you get `status 128` error, you have a problem with your gitlab ssh authentication.

https://docs.gitlab.com/ee/ssh/

If you get 
```503 Server Error: Service Temporarily Unavailable for url: https://gitlab.com/users/sign_in```, you may have an error in your repository 